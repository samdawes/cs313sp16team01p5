package com.oreilly.demo.android.pa.uidemo.test.android;

import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.View;

import com.oreilly.demo.android.pa.uidemo.TouchMe;
import com.oreilly.demo.android.pa.uidemo.constants.Constants;
import com.oreilly.demo.android.pa.uidemo.model.MonsterActivity;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by vsejk_000 (Uljana Sejko) on 4/28/2016.
 */
public abstract class AbstractTouchMeTest {// Will verify tht the activity can be launched

    private final int z = Constants.grid_sz;
    @Test
    public void testActivityCheckTestCaseSetUpProperly() {
        assertNotNull("The activity should be launched successfully", getActivity());
    }
    /* - This test will verify that onTouch() removes a monster iff it matches the location and is Vulnerable
     * @Test
      * public void testOnTouch(int[] location) throws Throwable{
      * float[] spot = findCoordinates(Location, DotView.findViewById(R.id.dots));
      * stimulateEventDown(View.findViewById(R.id.dots), spot[0], spot[1]);
      * }
      */
    /*Verifies that removeMonster(Location) works for both vulnerable and invulnerable monsters
     @throws Throwable
        */

    @Test
    public void testRemoveVulnerableMonster(){
        int[] m = getMonster(1);
        monsterActivityActivity.removeMonster(m[0], m[1]);
        assertTrue(getMonsterStatus(m) == 0);
    }
    @Test
    public void testRemoveInvulnerableMonster(){
        int[] m = getMonster(2);
        monsterActivityActivity.removeMonster(m[0], m[1]);
        assertTrue(getMonsterStatus(m) == 2);
    } //  getter + setter methods for easier testing */
    protected abstract TouchMe getActivity();
    public MonsterActivity monsterActivityActivity = new MonsterActivity();

    void simulateEventDown(View v, long m, long n) {
        MotionEvent o = MotionEvent.obtain(SystemClock.uptimeMillis(),SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, m, n, 0);
        v.dispatchTouchEvent(o);
    }
    float[] findCoordinations(int[] location, View v) {
        float[] f = {location[0] * v.getWidth() / z, location[1] * v.getHeight() / z};
        return f;
    }
    protected int getMonsterStatus(int[] location){
        return monsterActivityActivity.getMonsterMatrix()[location[0]][location[1]];
    }

    protected int[] getMonster(int status){ // This will get the first monster that matches the vulnerable or invulnerable status
        int temp[] = {-1, -1};
        int [][] t = monsterActivityActivity.getMonsterMatrix();
        for (int i = 0; i < t.length; i++)
            for (int j = 0; j < t.length; j++) {
                if (t[i][j] == status) {
                    temp[0] = i;
                    temp[1] = j;
                    break;
                }
            }
        return temp;
    }
}


