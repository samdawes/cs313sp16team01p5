package com.oreilly.demo.android.pa.uidemo.constants;

/**
 * Created by vsejk_000(Uljana Sejko) on 4/28/2016.
 */
public class Constants {

    public static int grid_sz = 10; // The dimension n by n for the game's grid.

    public static int round1_time = 120; // This is the time limit for round one.

    private Constants () {
    }
}