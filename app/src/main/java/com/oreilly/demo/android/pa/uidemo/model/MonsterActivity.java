package com.oreilly.demo.android.pa.uidemo.model;

import java.util.Random;
import static com.oreilly.demo.android.pa.uidemo.constants.Constants.grid_sz;

/**
 * Created by Madison on 4/28/2016.
 */
public class MonsterActivity {
    //size of grid
    private int g = grid_sz;
    //matrix of monsters
    private int [][] monsterMatrix = new int[g][g];
    //number of monsters
    int numMonsters = 0;

    public int getNumMonsters(){
        return numMonsters;
    }

    public MonsterActivity(){
        Random r = new Random();
        for(int i =0; i<g; i++) {
            for (int j = 0; j < g; j++) {
                int x = r.nextInt(3);
                //to fill about 1/3 of board
                if (x == 2) {
                    x = 0;
                }
                if (x == 1) {
                    numMonsters++;
                }
                monsterMatrix[i][j] = x;
            }
        }
    }

    public int[][] getMonsterMatrix(){
        return monsterMatrix;
    }

    public void setMonsterMatrix(int[][] m){
        this.monsterMatrix = m;
    }

    public int[][] moveMonster() {
        int newX, newY, x, y, z;
        Random r = new Random();
        int currentMonster = 0;
        int[][] skipList = new int[g][g];
        for (int i = 0; i < g; i++) {
            for (int j = 0; j < g; j++) {
                z = 0;
                //checking if there's a monster
                if ((monsterMatrix[i][j] == 1 || monsterMatrix[i][j] == 2) && skipList[i][j] != 1) {
                    //checking if there's a monster
                    if (monsterMatrix[i][j] == 1 || monsterMatrix[i][j] == 2) {
                        while (z != 3) {
                            //randomly pick 2 numbers
                            //send monster to i+x, i+y
                            //check if that spot is 1 or 2 already and randomly select again if it is

                            // pick a number, 0-4
                            // if it is 0 or 1, monster is not vulnerabl.
                            // if it is 2, monster is vulnerable
                            currentMonster = r.nextInt(6);
                            //if number picked is 0, set it 1
                            if (currentMonster == 0 || currentMonster > 2) {
                                currentMonster = 1;
                            }
                            x = r.nextInt(3);
                            if (x == 2) {
                                x = -1;
                            }
                            y = r.nextInt(3);
                            if (y == 2) {
                                y = -1;
                            }
                            newX = i + x;
                            newY = j + y;
                            //if x or y are zero, and i or j are zero
                            //Make sure not to make it a negative index!
                            if (newX == -1) {
                                newX = g - 1;
                            }
                            //if x or y are zero, and i or j are zero
                            // Make sure to not make it a negative index!
                            if (newY == -1) {
                                newY = g - 1;
                            }
                            //only tries 3 times, in case all neighbors are full.
                            if ((monsterMatrix[(newX) % g][(newY) % g] != 1) && (monsterMatrix[(newX) % g][(newY) % g] != 2)) {
                                //space is open so monster moves to it
                                monsterMatrix[(newX) % g][(newY) % g] = currentMonster;
                                //remove monster
                                monsterMatrix[i][j] = 0;
                                //putting a marker so if its moved, doesn't get moved again
                                skipList[(newX) % g][(newY) % g] = 1;
                                z = 3;
                            } else {
                                z++;
                            }
                        }
                    }
                }
            }
        }
        return monsterMatrix;
    }

        public void removeMonster(int x, int y){
            monsterMatrix[x][y] = 0;
    }

}

