package com.oreilly.demo.android.pa.uidemo.model.clock;


public interface OnTickListener {
    public void onTick();
}