package com.oreilly.demo.android.pa.uidemo.model.clock;

/**
 * The passive data model of the stopwatch that was originally from project 4
 * It does not emit any events.
 *
 *
 * @author laufer
 */
public interface TimeModel {
    void resetRuntime();
    //void incRuntime();
    void decRuntime();
    public int getRuntime();
    void setRuntime(int time);

}