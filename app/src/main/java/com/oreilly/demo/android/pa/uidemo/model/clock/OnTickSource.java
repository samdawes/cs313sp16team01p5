package com.oreilly.demo.android.pa.uidemo.model.clock;


public interface OnTickSource {
    void setOnTickListener(OnTickListener listener);
}