package com.oreilly.demo.android.pa.uidemo.model.clock;

/**
 * Created by Krunal on 12/10/15.
 */
public interface ClockModel extends OnTickSource {
    void start();
    void stop();
}